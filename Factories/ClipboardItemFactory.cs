﻿using ModuleManagementModule.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleManagementModule.Factories
{
    public static class ClipboardItemFactory 
    {
        public static List<ClipboardItem> GetClipboardItems(List<clsObjectRel> relatedItems)
        {
            var clipboardItems = relatedItems.Select(relItm => new ClipboardItem(relItm)).ToList();

            return clipboardItems;
        }
    }
}
