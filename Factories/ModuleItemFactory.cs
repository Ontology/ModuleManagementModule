﻿using ModuleManagementModule.Models;
using ModuleManagementModule.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleManagementModule.Factories
{
    public static class ModuleItemFactory
    {
        public static List<ViewModuleItem> GetModuleItems(List<ViewMetaItem> viewItems, ResultViewToClassRel resultViewToClassRel, clsOntologyItem oItemObject, clsLocalConfig localConfig, string baseUrl)
        {
            var viewItemList = new List<ViewModuleItem>();
            viewItems.ForEach(view =>
            {
                view.ModuleFunctions.ForEach(moduleFunction =>
                {
                    var viewAndModules = (from viewToClass in resultViewToClassRel.ViewsToClasses
                                          join viewToModule in resultViewToClassRel.ViewsToModules on viewToClass.ID_Object equals viewToModule.ID_Object
                                          select new { viewToClass, viewToModule }).ToList();

                    var moduleToClass = viewAndModules.Where(viewMod => viewMod.viewToModule.ID_Object == view.IdView && (oItemObject == null ? viewMod.viewToClass.ID_Other == localConfig.OItemModule.GUID : viewMod.viewToClass.ID_Other == oItemObject.GUID_Parent)).ToList();

                    var count = 0;

                    moduleToClass.ForEach(mod =>
                    {
                        count += (int)mod.viewToClass.OrderID;
                    });

                    viewItemList.Add(new ViewModuleItem
                    {
                        IdRow = localConfig.Globals.NewGUID,
                        IdView = view.IdView,
                        NameView = view.NameView,
                        IdModule = view.IdModule,
                        NameModule = view.NameModule,
                        IdModuleFunction = moduleFunction.GUID,
                        NameModuleFunction = moduleFunction.Name,
                        Url = baseUrl + (baseUrl.EndsWith("/") || view.NameCommandLineRun.StartsWith("/") ? "" : "/") + view.NameCommandLineRun + (oItemObject != null ? "?Object=" + oItemObject.GUID : ""),
                        OrderId = count
                    });
                });
            });

            return viewItemList.OrderByDescending(mod => mod.OrderId).ToList();
        }
    }
}
