﻿using ModuleManagementModule.Notifications;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleManagementModule.Models
{
    public class ViewModuleItem : NotifyPropertyChange
    {

        private string idRow;
        [DataViewColumn(Caption = "IdRow", IsIdField = true, IsVisible = false)]
        [Json()]
        public string IdRow
        {
            get { return idRow; }
            set
            {
                if (idRow == value) return;

                idRow = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IDRow);
            }
        }

        private string idView;

        [DataViewColumn(Caption = "IdView", IsIdField = false, IsVisible = false)]
        [Json()]
        public string IdView
        {
            get { return idView; }
            set
            {
                if (idView == value) return;

                idView = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdView);

            }
        }

        private string nameView;

        [DataViewColumn(Caption = "View", CellType = CellType.String, DisplayOrder = 0, IsIdField = false, IsVisible = true)]
        [Json()]
        public string NameView
        {
            get { return nameView; }
            set
            {
                if (nameView == value) return;

                nameView = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameView);

            }
        }

        private string idModule;
        
		[DataViewColumn(Caption = "IdModule", IsIdField = false, IsVisible = false)]
        [Json()]
        public string IdModule
        {
            get { return idModule; }
            set
            {
                if (idModule == value) return;

                idModule = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdModule);

            }
        }

        private string nameModule;
        
		[DataViewColumn(Caption = "Module", CellType = CellType.String, DisplayOrder = 1, IsIdField = false, IsVisible = true)]
        [Json()]
        public string NameModule
        {
            get { return nameModule; }
            set
            {
                if (nameModule == value) return;

                nameModule = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameModule);

            }
        }

        private string idModuleFunction;
        
		[DataViewColumn(Caption = "IdModuleFunction", CellType = CellType.String, IsIdField = false, IsVisible = false)]
        [Json()]
        public string IdModuleFunction
        {
            get { return idModuleFunction; }
            set
            {
                if (idModuleFunction == value) return;

                idModuleFunction = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdModuleFunction);

            }
        }

        private string nameModuleFunction;
        
		[DataViewColumn(Caption = "Function", CellType = CellType.String, DisplayOrder = 2, IsIdField = false, IsVisible = true)]
        [Json()]
        public string NameModuleFunction
        {
            get { return nameModuleFunction; }
            set
            {
                if (nameModuleFunction == value) return;

                nameModuleFunction = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameModuleFunction);

            }
        }

        private string version;
        
		[DataViewColumn(Caption = "Version", CellType = CellType.String, DisplayOrder = 3, IsIdField = false, IsVisible = true)]
        [Json()]
        public string Version
        {
            get { return version; }
            set
            {
                if (version == value) return;

                version = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Version);

            }
        }

        private int orderId;
        
		[DataViewColumn(Caption = "Order-Id", CellType = CellType.Integer, DisplayOrder = 4, IsIdField = false, IsVisible = true)]
        [Json()]
        public int OrderId
        {
            get { return orderId; }
            set
            {
                if (orderId == value) return;

                orderId = value;

                RaisePropertyChanged(NotifyChanges.DataModel_OrderId);

            }
        }

        private long? major;
        
		[DataViewColumn(Caption = "Major", CellType = CellType.Integer, IsIdField = false, IsVisible = false)]
        [Json()]
        public long? Major
        {
            get { return major; }
            set
            {
                if (major == value) return;

                major = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Major);

            }
        }

        private long? minor;
        
		[DataViewColumn(Caption = "Minor", CellType = CellType.Integer, IsIdField = false, IsVisible = false)]
        [Json()]
        public long? Minor
        {
            get { return minor; }
            set
            {
                if (minor == value) return;

                minor = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Minor);

            }
        }

        private long? build;
        
		[DataViewColumn(Caption = "Build", CellType = CellType.Integer, IsIdField = false, IsVisible = false)]
        [Json()]
        public long? Build
        {
            get { return build; }
            set
            {
                if (build == value) return;

                build = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Build);

            }
        }

        private long? revision;
        
		[DataViewColumn(Caption = "Revision", CellType = CellType.Integer, IsIdField = false, IsVisible = false)]
        [Json()]
        public long? Revision
        {
            get { return revision; }
            set
            {
                if (revision == value) return;

                revision = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Revision);
                    
            }
        }

        private string url;
        [DataViewColumn(Caption = "Link", CellType = CellType.Integer, IsIdField = false, IsVisible = true )]
        [Json()]
        public string Url
        {
            get { return url; }
            set
            {
                if (url == value) return;
                url = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Url);
            }
        }

    }
}
