﻿using ModuleManagementModule.Notifications;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleManagementModule.Models
{
    public class ClipboardItem : NotifyPropertyChange
    {
        private string rowId;
		[DataViewColumn(Caption = "RowId", CellType = CellType.String, IsIdField = true, IsVisible = false)]
        [Json()]
        public string RowId
        {
            get { return rowId; }
            set
            {
                if (rowId == value) return;

                rowId = value;

                RaisePropertyChanged(NotifyChanges.DataModel_RowId);

            }
        }

        private string id;
		[DataViewColumn(Caption = "Id", CellType = CellType.String, IsIdField = false, IsVisible = false)]
        [Json()]
        public string Id
        {
            get { return id; }
            set
            {
                if (id == value) return;

                id = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Id);

            }
        }

        private string nameClipboardItem;
		[DataViewColumn(Caption = "Name", CellType = CellType.String, DisplayOrder = 0, IsIdField = false, IsVisible = true)]
        [Json()]
        public string NameClipboardItem
        {
            get { return nameClipboardItem; }
            set
            {
                if (nameClipboardItem == value) return;

                nameClipboardItem = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameClipboardItem);

            }
        }

        private string parentClipboardItem;
		[DataViewColumn(Caption = "Parent", CellType = CellType.String, DisplayOrder = 1, IsIdField = false, IsVisible = true)]
        [Json()]
        public string ParentClipboardItem
        {
            get { return parentClipboardItem; }
            set
            {
                if (parentClipboardItem == value) return;

                parentClipboardItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ParentClipboardItem);

            }
        }

        private string typeClipboardItem;
		[DataViewColumn(Caption = "Type", CellType = CellType.String, DisplayOrder = 2, IsIdField = false, IsVisible = true)]
        [Json()]
        public string TypeClipboardItem
        {
            get { return typeClipboardItem; }
            set
            {
                if (typeClipboardItem == value) return;

                typeClipboardItem = value;

                RaisePropertyChanged(NotifyChanges.DataModel_TypeClipboardItem);

            }
        }

        private int orderId;
		[DataViewColumn(Caption = "OrderId", CellType = CellType.Integer, DisplayOrder = 3, IsIdField = false, IsVisible = true)]
        [Json()]
        public int OrderId
        {
            get { return orderId; }
            set
            {
                if (orderId == value) return;

                orderId = value;

                RaisePropertyChanged(NotifyChanges.DataModel_OrderId);

            }
        }

        public ClipboardItem(clsObjectRel relItem)
        {
            RowId = Guid.NewGuid().ToString();
            Id = relItem.ID_Other;
            NameClipboardItem = relItem.Name_Other;
            ParentClipboardItem = relItem.Name_Parent_Other;
            OrderId = (int)relItem.OrderID.Value;
            TypeClipboardItem = relItem.Ontology;
        }
    }
}
