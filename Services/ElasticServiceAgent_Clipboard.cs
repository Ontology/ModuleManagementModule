﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ModuleManagementModule.Services
{
    public delegate void AddedToClipboard(List<clsObjectRel> itemList);

    public class ElasticServiceAgent_Clipboard : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private clsOntologyItem oItemRef;
        private Thread getClipboardItems;

        public event AddedToClipboard addedToClipboard;

        private clsRelationConfig relationManager;

        private object lockerReadClipboardItems = new object();
        private object lockerWriteClipboardItems = new object();

        private List<clsObjectRel> clipboardItems;
        public List<clsObjectRel> ClipBoardItems
        {
            get
            {
                lock (lockerReadClipboardItems)
                {
                    return clipboardItems;
                }

            }
            set
            {
                lock (lockerReadClipboardItems)
                {
                    clipboardItems = value;
                }

                RaisePropertyChanged(Notifications.NotifyChanges.ElasticServiceAgent_ClipBoardItems);
            }
        }

        public clsOntologyItem GetClipboardItems(clsOntologyItem oItemRef)
        {
            this.oItemRef = oItemRef;
            StopReadModuleToClass();

            getClipboardItems = new Thread(GetClipboardItemsAsync);
            getClipboardItems.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetClipboardItemsAsync()
        {
            var dbReaderClipboardItems = new OntologyModDBConnector(localConfig.Globals);
            var result = localConfig.Globals.LState_Success.Clone();
            if (oItemRef != null)
            {
                if (oItemRef.Type == localConfig.Globals.Type_AttributeType)
                {
                    var searchRelated = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = localConfig.OItem_BaseConfig.GUID,
                    ID_RelationType = localConfig.Globals.RelationType_belongingAttribute.GUID,
                    Ontology = localConfig.Globals.Type_AttributeType
                }
            };

                    result = dbReaderClipboardItems.GetDataObjectRel(searchRelated);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        ClipBoardItems = dbReaderClipboardItems.ObjectRels;
                    }
                    else
                    {
                        ClipBoardItems = null;
                    }
                }
                else if (oItemRef.Type == localConfig.Globals.Type_RelationType)
                {
                    var searchRelated = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = localConfig.OItem_BaseConfig.GUID,
                    ID_RelationType = localConfig.Globals.RelationType_belongingRelationType.GUID,
                    Ontology = localConfig.Globals.Type_RelationType
                }
            };

                    result = dbReaderClipboardItems.GetDataObjectRel(searchRelated);
                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        ClipBoardItems = dbReaderClipboardItems.ObjectRels;
                    }
                    else
                    {
                        ClipBoardItems = null;
                    }
                }
                else if (oItemRef.Type == localConfig.Globals.Type_Object)
                {
                    var searchRelated = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = localConfig.OItem_BaseConfig.GUID,
                        ID_RelationType = localConfig.Globals.RelationType_belongingObject.GUID,
                        Ontology = localConfig.Globals.Type_Object
                    }
                };

                    result = dbReaderClipboardItems.GetDataObjectRel(searchRelated);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        if (!string.IsNullOrEmpty(oItemRef.GUID_Parent))
                        {
                            ClipBoardItems = dbReaderClipboardItems.ObjectRels.Where(clipItm => clipItm.ID_Parent_Other == oItemRef.GUID_Parent).ToList();
                        }
                        else
                        {
                            ClipBoardItems = dbReaderClipboardItems.ObjectRels;
                        }

                    }
                    else
                    {
                        ClipBoardItems = null;
                    }


                }
                else if (oItemRef.Type == localConfig.Globals.Type_Class)
                {
                    var searchRelated = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = localConfig.OItem_BaseConfig.GUID,
                        ID_RelationType = localConfig.Globals.RelationType_belongingClass.GUID,
                        ID_Parent_Other = oItemRef.GUID,
                        Ontology = localConfig.Globals.Type_Class
                    }
                };

                    result = dbReaderClipboardItems.GetDataObjectRel(searchRelated);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        ClipBoardItems = dbReaderClipboardItems.ObjectRels;
                    }
                    else
                    {
                        ClipBoardItems = null;
                    }
                }
            }
            else
            {
                var searchRelated = new List<clsObjectRel>
        {
            new clsObjectRel
            {
                ID_Object = localConfig.OItem_BaseConfig.GUID,
                ID_RelationType = localConfig.Globals.RelationType_belongingAttribute.GUID,
            },
            new clsObjectRel
            {
                ID_Object = localConfig.OItem_BaseConfig.GUID,
                ID_RelationType = localConfig.Globals.RelationType_belongingRelationType.GUID,
            },
            new clsObjectRel
            {
                ID_Object = localConfig.OItem_BaseConfig.GUID,
                ID_RelationType = localConfig.Globals.RelationType_belongingObject.GUID,
            },
            new clsObjectRel
            {
                ID_Object = localConfig.OItem_BaseConfig.GUID,
                ID_RelationType = localConfig.Globals.RelationType_belongingClass.GUID,
            }

        };

                result = dbReaderClipboardItems.GetDataObjectRel(searchRelated);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    ClipBoardItems = dbReaderClipboardItems.ObjectRels;
                }
                else
                {
                    ClipBoardItems = null;
                }
            }


        }

        public clsOntologyItem RemoveItems(List<string> clipboardItemIds)
        {
            var removeItemsAsync = new System.Threading.Thread(RemoveItemsAsync);
            removeItemsAsync.Start(clipboardItemIds);

            return localConfig.Globals.LState_Success.Clone();
        }

        private void RemoveItemsAsync(object clipboardItems)
        {
            var items = (List<string>)clipboardItems;
            var result = localConfig.Globals.LState_Success.Clone();
            var newClipboardList = ClipBoardItems;
            lock (lockerWriteClipboardItems)
            {
                if (ClipBoardItems != null && ClipBoardItems.Any())
                {
                    var toDelete = (from clipItm in ClipBoardItems
                                    join idItm in items on clipItm.ID_Other equals idItm
                                    select clipItm).ToList();

                    if (toDelete.Any())
                    {
                        var dbWriter = new OntologyModDBConnector(localConfig.Globals);
                        result = dbWriter.DelObjectRels(toDelete);
                    }

                    if (result.GUID != localConfig.Globals.LState_Error.GUID)
                    {
                        newClipboardList = (from clipItm in ClipBoardItems
                                            join deleted in toDelete on clipItm.ID_Other equals deleted.ID_Other into deletedItems
                                            from deleted in deletedItems.DefaultIfEmpty()
                                            where deleted == null
                                            select clipItm).ToList();
                    }

                }
            }

            if (result.GUID != localConfig.Globals.LState_Error.GUID)
            {
                ClipBoardItems = newClipboardList;
            }
        }



        public clsOntologyItem ClearClipboard()
        {
            var clearClipboardAsync = new System.Threading.Thread(ClearClipboardAsync);
            clearClipboardAsync.Start();

            return localConfig.Globals.LState_Success.Clone();
        }



        private void ClearClipboardAsync()
        {
            var result = localConfig.Globals.LState_Success.Clone();
            lock (lockerWriteClipboardItems)
            {
                if (ClipBoardItems != null && ClipBoardItems.Any())
                {
                    var dbWriter = new OntologyModDBConnector(localConfig.Globals);
                    result = dbWriter.DelObjectRels(ClipBoardItems);
                }
            }

            if (result.GUID != localConfig.Globals.LState_Error.GUID)
            {
                ClipBoardItems = new List<clsObjectRel>();
            }
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReaderOItem = new OntologyModDBConnector(localConfig.Globals);

            return dbReaderOItem.GetOItem(id, type);
        }

        public void StopRead()
        {
            StopReadModuleToClass();
        }

        private void StopReadModuleToClass()
        {
            if (getClipboardItems != null)
            {
                try
                {
                    getClipboardItems.Abort();
                }
                catch (Exception ex)
                {

                }
            }
        }

        public clsOntologyItem AddToClipbaord(List<clsOntologyItem> itemList)
        {
            var addToClipboardAsync = new System.Threading.Thread(AddToClipboardAsync);
            addToClipboardAsync.Start(itemList);

            return localConfig.Globals.LState_Success.Clone();
        }

        private void AddToClipboardAsync(object itemList)
        {
            lock (lockerWriteClipboardItems)
            {
                var items = (List<clsOntologyItem>)itemList;
                var addedList = new List<clsObjectRel>();
                var dbWriter = new OntologyModDBConnector(localConfig.Globals);
                var dbReader = new OntologyModDBConnector(localConfig.Globals);

                if (items != null && items.Any())
                {
                    var attributeTypeItems = items.Where(itm => itm.Type == localConfig.Globals.Type_AttributeType).Select(itm => relationManager.Rel_ObjectRelation(localConfig.OItem_BaseConfig, itm, localConfig.Globals.RelationType_belongingAttribute)).ToList();
                    var relationTypeItems = items.Where(itm => itm.Type == localConfig.Globals.Type_RelationType).Select(itm => relationManager.Rel_ObjectRelation(localConfig.OItem_BaseConfig, itm, localConfig.Globals.RelationType_belongingRelationType)).ToList();
                    var classItems = items.Where(itm => itm.Type == localConfig.Globals.Type_Class).Select(itm => relationManager.Rel_ObjectRelation(localConfig.OItem_BaseConfig, itm, localConfig.Globals.RelationType_belongingClass)).ToList();
                    var objectItems = items.Where(itm => itm.Type == localConfig.Globals.Type_Object).Select(itm => relationManager.Rel_ObjectRelation(localConfig.OItem_BaseConfig, itm, localConfig.Globals.RelationType_belongingObject)).ToList();

                    if (attributeTypeItems.Any())
                    {
                        var result = dbWriter.SaveObjRel(attributeTypeItems);
                        if (result.GUID != localConfig.Globals.LState_Error.GUID)
                        {
                            addedList.AddRange(attributeTypeItems);
                        }
                    }

                    if (relationTypeItems.Any())
                    {
                        var result = dbWriter.SaveObjRel(relationTypeItems);
                        if (result.GUID != localConfig.Globals.LState_Error.GUID)
                        {
                            addedList.AddRange(relationTypeItems);
                        }
                    }

                    if (classItems.Any())
                    {
                        var result = dbWriter.SaveObjRel(classItems);
                        if (result.GUID != localConfig.Globals.LState_Error.GUID)
                        {
                            addedList.AddRange(classItems);
                        }
                    }

                    if (objectItems.Any())
                    {
                        var result = dbWriter.SaveObjRel(objectItems);
                        if (result.GUID != localConfig.Globals.LState_Error.GUID)
                        {
                            addedList.AddRange(objectItems);
                        }
                    }

                    if (addedList.Any())
                    {
                        var result = dbReader.GetDataObjectRel(addedList);
                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            addedList = dbReader.ObjectRels;
                        }
                    }
                    addedToClipboard?.Invoke(addedList);
                }
                else
                {
                    addedToClipboard?.Invoke(addedList);
                }
            }

        }

        public ElasticServiceAgent_Clipboard(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            relationManager = new clsRelationConfig(localConfig.Globals);
        }
    }

}
