﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ModuleManagementModule.Services
{
    public delegate void SavedModuleToClassRel(bool withoutError, string idView, long orderId);
    public class ElasticServiceAgent_ModuleStarter : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;


        private clsTransaction transactionManager;
        private clsRelationConfig relationManager;

        private OntologyModDBConnector dbReaderModuleToClass;

        public event SavedModuleToClassRel savedModuleToClassRel;

        private Thread getModuleToClassAsync;
        private object lockerReadModuleToClass = new object();
        private object lockerWriteModuleToClass = new object();

        private ResultViewToClassRel viewToClassRel;
        public ResultViewToClassRel ViewToClassRel
        {
            get
            {
            
                lock(lockerReadModuleToClass)
                {

                    return viewToClassRel;
                }    
                
            }
            set
            {
                lock(lockerReadModuleToClass)
                {
                    viewToClassRel = value;
                }
                RaisePropertyChanged(nameof(ViewToClassRel));
            }
        }
        public async Task<ResultViewToClassRel> GetViewToClassRel()
        {

            var result = new ResultViewToClassRel
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchViewToClass = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Parent_Object = localConfig.OItem_class_websocketview.GUID,
                    ID_RelationType = localConfig.Globals.RelationType_belongingClass.GUID
                }
            };

            var dbReaderViews = new OntologyModDBConnector(localConfig.Globals);

            var oResult = dbReaderViews.GetDataObjectRel(searchViewToClass);

            if (oResult.GUID == localConfig.Globals.LState_Error.GUID)
            {
                result.Result = oResult;
                ViewToClassRel = result;
                return result;
            }


            result.ViewsToClasses = dbReaderViews.ObjectRels;

            var searchViewToModule = dbReaderViews.ObjectRels.Select(viewToModule => new clsObjectRel
            {
                ID_Object = viewToModule.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                ID_Parent_Other = localConfig.OItem_type_module.GUID
            }).ToList();
            result.ViewsToModules = new List<clsObjectRel>();
            if (searchViewToModule.Any())
            {
                var dbReaderModules = new OntologyModDBConnector(localConfig.Globals);
                oResult = dbReaderModules.GetDataObjectRel(searchViewToModule);
                result.ViewsToModules = dbReaderModules.ObjectRels;
            }
            
            

            result.Result = oResult;
            ViewToClassRel = result;   
            return result;
        }


        public async Task<clsOntologyItem> SaveClassToView(clsOntologyItem oItemClass, clsOntologyItem oItemView, long orderId)
        {

            var dbWriter = new OntologyModDBConnector(localConfig.Globals);
            var result = localConfig.Globals.LState_Success.Clone();
            lock (lockerWriteModuleToClass)
            {
                result = localConfig.Globals.LState_Success.Clone();
                var relToSave = relationManager.Rel_ObjectRelation(oItemView, oItemClass, localConfig.Globals.RelationType_belongingClass, orderId: orderId);

                transactionManager.ClearItems();
                result = transactionManager.do_Transaction(relToSave);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    if (ViewToClassRel != null && ViewToClassRel.ViewsToClasses != null)
                    {
                        var viewToClass = ViewToClassRel.ViewsToClasses.FirstOrDefault(viewToClassItm => viewToClassItm.ID_Object == oItemView.GUID && viewToClassItm.ID_Other == oItemClass.GUID);
                        if (viewToClass == null)
                        {
                            ViewToClassRel.ViewsToClasses.Add(new clsObjectRel
                            {
                                ID_Object = oItemView.GUID,
                                ID_Other = oItemClass.GUID,
                                ID_RelationType = localConfig.Globals.RelationType_belongingClass.GUID,
                                OrderID = orderId
                            });
                        }
                    }
                }
                savedModuleToClassRel?.Invoke(result.GUID == localConfig.Globals.LState_Success.GUID, oItemView.GUID, orderId);
            }

            return result;
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReaderOItem = new OntologyModDBConnector(localConfig.Globals);

            return dbReaderOItem.GetOItem(id, type);
        }

        public ElasticServiceAgent_ModuleStarter(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            dbReaderModuleToClass = new OntologyModDBConnector(localConfig.Globals);
            relationManager = new clsRelationConfig(localConfig.Globals);
            transactionManager = new clsTransaction(localConfig.Globals);
        }
    }

    public class ResultViewToClassRel
    {
        public clsOntologyItem Result { get; set; }
        public List<clsObjectRel> ViewsToClasses { get; set; }
        public List<clsObjectRel> ViewsToModules { get; set; }
    }
}
