﻿using ModuleManagementModule.Factories;
using ModuleManagementModule.Models;
using ModuleManagementModule.Services;
using ModuleManagementModule.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Attributes;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ModuleManagementModule.Controllers
{
    public class ClipboardViewController : ClipboardViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ElasticServiceAgent_Clipboard serviceAgent_Elastic;

        private clsLocalConfig localConfig;

        private clsOntologyItem oItemSelected;

        private string fileNameGrid;

        private List<ClipboardItem> clipboardItems;
        private List<ClipboardItem> clipboardItemPreApplied = new List<ClipboardItem>();

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public ClipboardViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();

            PropertyChanged += ClipboardViewController_PropertyChanged;

        }

        private void ClipboardViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgent_Elastic = new ElasticServiceAgent_Clipboard(localConfig);
            serviceAgent_Elastic.PropertyChanged += ServiceAgent_Elastic_PropertyChanged;
            serviceAgent_Elastic.addedToClipboard += ServiceAgent_Elastic_addedToClipboard;



        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();

            serviceAgent_Elastic = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
            //if (webSocketServiceAgent.Request.ContainsKey("ListType"))
            //{

            //}
        }

        private void StateMachine_loginSucceded()
        {
           

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.SelectedClassNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AppliedObjects,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AddToClipboard,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            IsToggled_Listen = true;

            serviceAgent_Elastic.GetClipboardItems(null);




            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ServiceAgent_Elastic_addedToClipboard(List<clsObjectRel> itemList)
        {
            var clipItems = ClipboardItemFactory.GetClipboardItems(itemList);
            var viewItem = GridFactory.CreateViewItem(clipItems.ToList<object>(), typeof(ClipboardItem), "grid", ViewItemClass.Grid.ToString(), ViewItemType.AddRow.ToString());
            clipboardItems.AddRange(clipItems);
            webSocketServiceAgent.SendViewItems(new List<ViewItem> { viewItem });
            
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
        }

      
        private void ServiceAgent_Elastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ElasticServiceAgent_ClipBoardItems)
            {
                if (serviceAgent_Elastic.ClipBoardItems != null)
                {
                    var reload = false;
                    clipboardItems = ClipboardItemFactory.GetClipboardItems(serviceAgent_Elastic.ClipBoardItems);
                    if (string.IsNullOrEmpty(fileNameGrid))
                    {
                        fileNameGrid = Guid.NewGuid().ToString() + ".json";
                        var columnConfig_Grid = GridFactory.CreateColumnList(typeof(ClipboardItem));
                        columnConfig_Grid.ColumnList.Insert(0, new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Checkbox)
                        {
                            datafield = "ItemApply",
                            hidden = false,
                            editable = true,
                            width = 50
                        });
                        ColumnConfig_Grid = columnConfig_Grid;
                    }
                    else
                    {
                        reload = true;
                    }
                    
                    

                    var jsonFactory = new JsonFactory();
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(fileNameGrid);
                    var result = jsonFactory.CreateJsonFileOfItemList(typeof(ClipboardItem), clipboardItems.ToList<object>(), sessionFile);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        if (!reload)
                        {
                            JqxDataSource_Grid = GridFactory.CreateJqxDataSource(typeof(ClipboardItem), null, null, sessionFile.FileUri.AbsolutePath);
                        }
                        else
                        {
                            webSocketServiceAgent.SendCommand("reloadGrid");
                        }

                    }
                }
            }
        }

      
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {
                

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "AppliedObjectRow")
                {
                    ToggleCheckObjectRow(true);
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "UnAppliedObjectRow")
                {
                    ToggleCheckObjectRow(false);

                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "AppliedObjectRows")
                {


                    AppliedObjectRows();


                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "ClearClipboard")
                {
                    ClearClipboard();
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "DeleteItems")
                {
                    DeletePreAppliedItems();
                }
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == "grid" && changedItem.ViewItemType == "SelectedIndex")
                    {
                        CellItem_CellSelected = Newtonsoft.Json.JsonConvert.DeserializeObject<JqxCellItem>(changedItem.ViewItemValue.ToString());
                    }
                });
            }



        }

        private void DeletePreAppliedItems()
        {
            if (clipboardItemPreApplied == null || !clipboardItemPreApplied.Any()) return;

            serviceAgent_Elastic.RemoveItems(clipboardItemPreApplied.Select(clipItm => clipItm.Id).ToList());
        }

        private void ClearClipboard()
        {
            serviceAgent_Elastic.ClearClipboard();
        }

        private void ToggleCheckObjectRow(bool isChecked)
        {

            if (isChecked)
            {
                var clipBoardItem = clipboardItems.FirstOrDefault(itm => itm.RowId == webSocketServiceAgent.Request["Id"].ToString());
                if (clipBoardItem != null)
                {
                    clipboardItemPreApplied.Add(clipBoardItem);
                }
            }
            else
            {
                clipboardItemPreApplied.RemoveAll(itm => itm.RowId == webSocketServiceAgent.Request["Id"].ToString());
            }
        }

        private void AppliedObjectRows()
        {
            if (clipboardItemPreApplied != null && clipboardItemPreApplied.Any())
            {
                var attributeTypeItems = (from clipboardItem in clipboardItemPreApplied.Where(itm => itm.TypeClipboardItem == localConfig.Globals.Type_AttributeType)
                                          join dbItem in serviceAgent_Elastic.ClipBoardItems on clipboardItem.Id equals dbItem.ID_Other
                                          select new clsOntologyItem
                                          {
                                              GUID = dbItem.ID_Other,
                                              Name = dbItem.Name_Other,
                                              GUID_Parent = dbItem.ID_Parent_Other,
                                              Type = localConfig.Globals.Type_AttributeType
                                          }).ToList();

                var relationTypeItems = (from clipboardItem in clipboardItemPreApplied.Where(itm => itm.TypeClipboardItem == localConfig.Globals.Type_RelationType)
                                          join dbItem in serviceAgent_Elastic.ClipBoardItems on clipboardItem.Id equals dbItem.ID_Other
                                         select new clsOntologyItem
                                         {
                                             GUID = dbItem.ID_Other,
                                             Name = dbItem.Name_Other,
                                             Type = localConfig.Globals.Type_RelationType
                                         }).ToList();

                var classItems = (from clipboardItem in clipboardItemPreApplied.Where(itm => itm.TypeClipboardItem == localConfig.Globals.Type_Class)
                                          join dbItem in serviceAgent_Elastic.ClipBoardItems on clipboardItem.Id equals dbItem.ID_Other
                                  select new clsOntologyItem
                                  {
                                      GUID = dbItem.ID_Other,
                                      Name = dbItem.Name_Other,
                                      GUID_Parent = dbItem.ID_Parent_Other,
                                      Type = localConfig.Globals.Type_Class
                                  }).ToList();

                var objectItems = (from clipboardItem in clipboardItemPreApplied.Where(itm => itm.TypeClipboardItem == localConfig.Globals.Type_Object)
                                   join dbItem in serviceAgent_Elastic.ClipBoardItems on clipboardItem.Id equals dbItem.ID_Other
                                   select new clsOntologyItem
                                   {
                                       GUID = dbItem.ID_Other,
                                       Name = dbItem.Name_Other,
                                       GUID_Parent = dbItem.ID_Parent_Other,
                                       Type = localConfig.Globals.Type_Object
                                   }).ToList();

                if (attributeTypeItems.Any())
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.AppliedAttributeTypes,
                        OItems = attributeTypeItems
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }

                if (relationTypeItems.Any())
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.AppliedRelationTypes,
                        OItems = relationTypeItems
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }

                if (classItems.Any())
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.AppliedClasses,
                        OItems = classItems
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }

                if (objectItems.Any())
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.AppliedObjects,
                        OItems = objectItems
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }
            }
        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {

            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList)
            {

            }
            else if (message.ChannelId == Channels.SelectedClassNode && IsToggled_Listen)
            {

                var oItemMessage = message.OItems.LastOrDefault();
                if (oItemMessage == null) return;
                if (oItemSelected != null && oItemMessage.GUID == oItemSelected.GUID) return;

                oItemSelected = oItemMessage;

                if (oItemSelected == null) return;

                var classItem = serviceAgent_Elastic.GetOItem(oItemSelected.GUID, localConfig.Globals.Type_Class);

                if (classItem == null) return;
                clipboardItemPreApplied.Clear();
                serviceAgent_Elastic.GetClipboardItems(classItem);
            }
            else if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                var oItemMessage = message.OItems.LastOrDefault();
                if (oItemMessage == null) return;
                if (oItemSelected != null && oItemMessage.GUID == oItemSelected.GUID) return;

                oItemSelected = message.OItems.LastOrDefault();

                if (oItemSelected == null) return;

                var objectItem = serviceAgent_Elastic.GetOItem(oItemSelected.GUID, localConfig.Globals.Type_Object);

                if (objectItem == null) return;
                clipboardItemPreApplied.Clear();
                serviceAgent_Elastic.GetClipboardItems(objectItem);
            }
            else if (message.ChannelId == Channels.AppliedObjects)
            {
                var oItems = message.OItems;

                if (oItems == null) return;

                var result = serviceAgent_Elastic.AddToClipbaord(oItems);
            }
            else if (message.ChannelId == Channels.AddToClipboard)
            {
                var oItems = message.OItems;

                if (oItems == null) return;

                var result = serviceAgent_Elastic.AddToClipbaord(oItems);
            }

        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
