﻿using ModuleManagementModule.Factories;
using ModuleManagementModule.Models;
using ModuleManagementModule.Services;
using ModuleManagementModule.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ModuleManagementModule.Controllers
{
    public class ModuleStarterViewController : ModuleStarterViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ElasticServiceAgent_ModuleStarter serviceAgent_Elastic;

        private clsLocalConfig localConfig;

        private clsOntologyItem oItemSelected;

        private List<ViewModuleItem> modules;

        private string fileNameGrid;

        private JqxCellItem cellItemToChange;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public ModuleStarterViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();

            PropertyChanged += ModuleStarterViewController_PropertyChanged;
        }

        private void ModuleStarterViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);

            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_CellItem_CellSelected && CellItem_CellSelected != null && modules != null)
            {
                if (CellItem_CellSelected.ColumnName == "Url")
                {
                    var moduleItem = modules.FirstOrDefault(mod => mod.IdRow == CellItem_CellSelected.Id);
                    if (oItemSelected != null)
                    {
                        clsOntologyItem classItem = null;
                        clsOntologyItem oItemView = serviceAgent_Elastic.GetOItem(moduleItem.IdView, localConfig.Globals.Type_Object);
                        if (oItemSelected.Type == localConfig.Globals.Type_Class)
                        {
                            classItem = oItemSelected;
                            
                        }
                        else if (oItemSelected.Type == localConfig.Globals.Type_Object)
                        {
                            classItem = serviceAgent_Elastic.GetOItem(oItemSelected.GUID_Parent, localConfig.Globals.Type_Class);
                        }

                        if (classItem != null && oItemView != null)
                        {
                            cellItemToChange = new JqxCellItem
                            {
                                ColumnName = "OrderId",
                                Id = CellItem_CellSelected.Id,
                                RowId = CellItem_CellSelected.RowId,
                                Value = (moduleItem.OrderId + 1).ToString()
                            };
                            var result = serviceAgent_Elastic.SaveClassToView(classItem, oItemView, moduleItem.OrderId + 1);
                        }
                    }
                    else
                    {
                        clsOntologyItem classItem = localConfig.OItemModule;
                        clsOntologyItem oItemView = serviceAgent_Elastic.GetOItem(moduleItem.IdView, localConfig.Globals.Type_Object);
                        cellItemToChange = new JqxCellItem
                        {
                            ColumnName = "OrderId",
                            Id = CellItem_CellSelected.Id,
                            RowId = CellItem_CellSelected.RowId,
                            Value = (moduleItem.OrderId + 1).ToString()
                        };
                        var result = serviceAgent_Elastic.SaveClassToView(classItem, oItemView, moduleItem.OrderId + 1);

                    }
                }
                

            }
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgent_Elastic = new ElasticServiceAgent_ModuleStarter(localConfig);
            serviceAgent_Elastic.PropertyChanged += ServiceAgent_Elastic_PropertyChanged;
            serviceAgent_Elastic.savedModuleToClassRel += ServiceAgent_Elastic_savedModuleToClassRel;



        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();

            serviceAgent_Elastic = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            //if (webSocketServiceAgent.Request.ContainsKey("ListType"))
            //{

            //}
        }

        private void StateMachine_loginSucceded()
        {
         

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.SelectedClassNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            IsToggled_Listen = true;



            var result = serviceAgent_Elastic.GetViewToClassRel();


         
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
        }

     
        private void ServiceAgent_Elastic_savedModuleToClassRel(bool withoutError, string idView, long orderId)
        {
            if (withoutError)
            {
                var viewItem = modules.FirstOrDefault(viewModule => viewModule.IdView == idView);
                if (viewItem != null) viewItem.OrderId = (int)orderId;
                CellItem_ChangedItem = cellItemToChange;
            }
            else
            {

            }
        }

        private void ServiceAgent_Elastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Services.ElasticServiceAgent_ModuleStarter.ViewToClassRel) && serviceAgent_Elastic.ViewToClassRel != null)
            {
                CreateGridConfig(oItemSelected);   
            }
        }

        private void CreateGridConfig(clsOntologyItem oItemObject = null)
        {
            modules = ModuleItemFactory.GetModuleItems(ModuleDataExchanger.GetViews(), serviceAgent_Elastic.ViewToClassRel, oItemObject, localConfig, webSocketServiceAgent.GetFileSystemObject().BaseUrl);
            var firstLoad = (ColumnConfig_Grid == null);
            if (firstLoad)
            {

                fileNameGrid = Guid.NewGuid().ToString() + ".json";
                ColumnConfig_Grid = GridFactory.CreateColumnList(typeof(ViewModuleItem));
            }
            
            var jsonFactory = new JsonFactory();
            var sessionFile = webSocketServiceAgent.RequestWriteStream(fileNameGrid);
            var result = jsonFactory.CreateJsonFileOfItemList(typeof(ViewModuleItem), modules.ToList<object>(), sessionFile);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (firstLoad)
                {
                    JqxDataSource_Grid = GridFactory.CreateJqxDataSource(typeof(ViewModuleItem), null, null, sessionFile.FileUri.AbsolutePath);
                }
                else
                {
                    webSocketServiceAgent.SendCommand("reloadGrid");
                }
                
            }
        }

      
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == "grid" && changedItem.ViewItemType == "SelectedIndex")
                    {
                        CellItem_CellSelected = Newtonsoft.Json.JsonConvert.DeserializeObject<JqxCellItem>(changedItem.ViewItemValue.ToString());
                    }
                });
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument)
            {
                var idObject = webSocketServiceAgent.ObjectArgument.Value;

                if (!localConfig.Globals.is_GUID(idObject)) return;

                var oItem = serviceAgent_Elastic.GetOItem(idObject, localConfig.Globals.Type_Object);

                if (oItem == null) return;

                oItemSelected = oItem;
                

                
            }


        }


        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            else if (message.ChannelId == Channels.SelectedClassNode)
            {

                var oItemMessage = message.OItems.LastOrDefault();
                if (oItemMessage == null) return;
                if (oItemSelected != null && oItemMessage.GUID == oItemSelected.GUID) return;

                oItemSelected = oItemMessage;

                if (oItemSelected == null) return;
               

            }
            else if (message.ChannelId == Channels.ParameterList)
            {
                var oItemMessage = message.OItems.LastOrDefault();
                if (oItemMessage == null) return;
                if (oItemSelected != null && oItemMessage.GUID == oItemSelected.GUID) return;

                oItemSelected = message.OItems.LastOrDefault();

                if (oItemSelected == null) return;

                var classItem = serviceAgent_Elastic.GetOItem(oItemSelected.GUID_Parent, localConfig.Globals.Type_Class);

                Text_View = oItemSelected.Name + " (" + classItem.Name + ")";

                var result = serviceAgent_Elastic.GetViewToClassRel();
                
            }
            
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
